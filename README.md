# Registration Task

Integracion Registra tareas


#### Control de versiones 
| Version | Etapa      | Fecha      |
| ------- | ---------- | ---------- |
| 1.0     | Desarrollo | 15-10-2020 |
|

### Aspectos Técnicos

Diseñado con las siguientes tecnologías:

* [Spring Boot] Microservicio REST
* [Hibernate JPA] Persistencia
* [H2] Base de Datos
* [lombok] Ultima Version.

### Configuracion de aplicacion

El servicio necesita configuraciones propias de la aplicación se implemento sobrep una BD h2 por lo que no necesita mayor configuracion, considerar que los datos seran persistido mientras exista una instancia de este servicio, la tabla se autogenera a partir de la entidad, de todas formas se adjunta scritp de tabla por si necesitan conectarlo alguna otra BD.

##### Configuracion de Ambiente
Definidas en application.properties 

Para la configuracion lombok en equipolocal agregar plugin a ide o seguir los pasos https://projectlombok.org/setup/eclipse o https://www.baeldung.com/lombok-ide, en servidor no es necesario realizar nada especial.
Se agrega configuracion basica para el uso Health Check.


##### Configuracion Openshift Editar en YAML

| Servicio | URL      |
| ------- | ---------- |
|http://localhost:8080/swagger-ui.html
 
 
