package com.registrationTask.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.registrationTask.entity.TaskEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AppResponse {

	List<TaskEntity> lTask;
}
