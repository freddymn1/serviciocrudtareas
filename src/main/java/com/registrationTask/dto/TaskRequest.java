
package com.registrationTask.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "idTask", "description" })
@Getter
@Setter
@ToString
public class TaskRequest {

	
	@JsonProperty("description")
	@NotEmpty(message = "Descripcion tarea es requerido")
	private String description;
	
	private boolean isActive;

}
