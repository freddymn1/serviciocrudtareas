package com.registrationTask.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationTaskResponse implements Serializable {

	private static final long serialVersionUID = -6207626164658348209L;

	@JsonProperty("status")
	private Integer status;

	@JsonProperty("message")
	private String message;
		
	@JsonProperty("data")
	private transient Object data;

	public RegistrationTaskResponse(Integer status, String message, Object data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public RegistrationTaskResponse() {
	}
	
}
