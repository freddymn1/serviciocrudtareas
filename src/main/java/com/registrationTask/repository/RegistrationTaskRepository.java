package com.registrationTask.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.registrationTask.entity.TaskEntity;

@Repository
@Transactional
public interface RegistrationTaskRepository extends CrudRepository<TaskEntity, Long> {

	
	@Modifying(clearAutomatically = true)
	@Query("SELECT t FROM TaskEntity t")
	public List<TaskEntity> findTask();
	
	
	
}
