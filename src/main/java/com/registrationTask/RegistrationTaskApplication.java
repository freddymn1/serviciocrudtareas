package com.registrationTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrationTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationTaskApplication.class, args);
	}

}
