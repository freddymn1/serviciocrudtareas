package com.registrationTask.utils;

import java.io.Serializable;

public class Constants implements Serializable {

	private static final long serialVersionUID = 6354630435599385052L;

	public Constants() {
		throw new IllegalStateException("Utility class");
	}

	public static final String FORMATO_FECHA = "dd/MM/yyyy";
	public static final int STATUS_OK = 200;
	public static final int STATUS_CREADO = 201;
	public static final int STATUS_NOT_FOUND = 404;
	public static final int STATUS_CONFLICT = 409;
	
	public static final String SUCCESS = "Registrado con Exito";
	public static final String SUCCESS_DEL = "Eliminado con Exito";
	public static final String SUCCESS_UPDATE = "Actualizadp con Exito";
	public static final String USUARIO_ACTIVO = "Activo";
	public static final String USUARIO_NOACTIVO = "No Activo";
	public static final String FAIL = "Proceso Fallido";
	public static final int STATUS_FAIL = 400;
	public static final String REGISTER_NOT_FOUND = "Registro no encontrado";
	public static final String REGISTER_EXIST = "Registro ya existe";
	
	
	
	
	
}
