package com.registrationTask.utils;

public enum InfoEnumUtil {

	INI_REGISTRA_TASK("001-INI REGISTRATION TASK CONTROLLER ", "Inicio controller registra tarea"),

	FIN_REGISTRA_TASK("002-FIN REGISTRATION TASK CONTROLLER ", "FIN controller regsitro de tarea"),

	INI_LISTA_TASK("003-INI LISTA TASK CONTROLLER ", "Inicio controller lista tarea"),

	FIN_LISTA_TASK("004-FIN LISTA TASK CONTROLLER ", "FIN controller lista de tarea"),

	INI_DEL_TASK("005-INI DEL TASK CONTROLLER ", "Inicio controller elimina tarea"),

	FIN_DEL_TASK("006-FIN DEL TASK CONTROLLER ", "FIN controller elimina tarea"),

	INI_UPDATE_TASK("007-INI DEL UPDATE CONTROLLER ", "Inicio controller actualiza tarea"),

	FIN_UPDATE_TASK("008-FIN DEL UPDATE CONTROLLER ", "FIN controller actualiza tarea"),

	INI_REGISTRA_TASK_SERVICE("009-INI REGISTRATION TASK SERVICE", "INI Services Registra tarea"),

	FIN_REGISTRA_TASK_SERVICE("010-FIN REGISTRATION TASK SERVICE", "FIN Services Registra tarea"),

	INI_LISTA_TASK_SERVICE("011-INI LISTA TASK SERVICE", "INI Services lista tarea"),

	FIN_LISTA_TASK_SERVICE("012-FIN LISTA TASK SERVICE", "FIN Services lista tarea"),

	INI_DEL_TASK_SERVICE("013-INI DEL TASK SERVICE", "INI Services elimina tarea"),

	FIN_DEL_TASK_SERVICE("014-FIN DEL TASK SERVICE", "FIN Services elimina tarea"),

	INI_UPDATE_TASK_SERVICE("015-INI UPDATE TASK SERVICE", "INI Services actualiza tarea"),

	FIN_UPDATE_TASK_SERVICE("016-FIN UPDATE TASK SERVICE", "FIN Services actualiza tarea");

	private final String code;
	private final String msg;

	InfoEnumUtil(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return this.code;
	}

	public String getMsg() {
		return this.msg;
	}
}