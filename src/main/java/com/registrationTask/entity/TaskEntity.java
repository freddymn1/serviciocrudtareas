package com.registrationTask.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Entity
@Getter
@Setter
@ToString
@Table(name = "TASK")

public class TaskEntity {

	@NotNull(message = "campo id es requerido")
	@ApiModelProperty(value = "1", name = "id", notes = "id de tarea", required = true)
	@Id
	@Column(name = "id")
	public Long id;

	@NotBlank(message = "campo descripcion es requerido")
	@ApiModelProperty(value = "1", name = "description", notes = "Descripcion Tarea", required = true)
	@Column(name = "description", nullable = false)
	private String description;

	@NotNull(message = "campo creationDate es requerido")
	@ApiModelProperty(value = "1", name = "creation Date", notes = "Fecha Creacion", required = true)
	@Column(name = "creationDate", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@NotNull(message = "campo isActivo es requerido")
	@ApiModelProperty(value = "true", name = "isActive", notes = " Registro activo", required = true)
	@Column(name = "isActive", nullable = false)
	private boolean isActive;
	
	
}
