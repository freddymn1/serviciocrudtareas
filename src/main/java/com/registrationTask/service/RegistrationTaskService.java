package com.registrationTask.service;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.registrationTask.dto.AppResponse;
import com.registrationTask.dto.RegistrationTaskResponse;
import com.registrationTask.entity.TaskEntity;
import com.registrationTask.exception.BusinessException;
import com.registrationTask.repository.RegistrationTaskRepository;
import com.registrationTask.utils.Constants;
import com.registrationTask.utils.ErrorEnumUtil;
import com.registrationTask.utils.InfoEnumUtil;
import com.registrationTask.utils.LogMessageUtil;

/**
 * Servicio para registrar tareas en BD H2
 * 
 * @author fpainenao
 *
 */

@Service
public class RegistrationTaskService {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationTaskService.class);

	@Autowired(required = true)
	RegistrationTaskRepository registrationUserRepository;

	/**
	 * registra Usuario en modelo H2
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public RegistrationTaskResponse addTask(TaskEntity task) throws ParseException {

		RegistrationTaskResponse response = new RegistrationTaskResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_REGISTRA_TASK_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		try {

			boolean existe = registrationUserRepository.existsById(task.getId());
			if (!existe) {
				registrationUserRepository.save(task);
				response.setMessage(Constants.SUCCESS);
				response.setStatus(Constants.STATUS_CREADO);

			} else {

				response.setMessage(Constants.REGISTER_EXIST);
				response.setStatus(Constants.STATUS_CONFLICT);
			}

			logger.info(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_REGISTRA_TASK_SERVICE.getCode(),
					task.toString());

		} catch (DataAccessException ex) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_REGISTRA_TASK_SERVICE.getCode(),
					ex.getLocalizedMessage());

			response.setMessage(Constants.FAIL);
			response.setStatus(Constants.STATUS_FAIL);
			throw new BusinessException(ErrorEnumUtil.ERROR_REGISTRAR_TASK);

		}

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_REGISTRA_TASK_SERVICE.getCode(), response);

		return response;
	}

	/**
	 * registra Usuario en modelo H2
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public RegistrationTaskResponse listAllTask() throws ParseException {

		RegistrationTaskResponse response = new RegistrationTaskResponse();
		AppResponse data = new AppResponse();

		try {

			List<TaskEntity> lTask = registrationUserRepository.findTask();
			data.setLTask(lTask);

		} catch (DataAccessException ex) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_LISTA_TASK_SERVICE.getCode(),
					ex.getLocalizedMessage());

			response.setMessage(Constants.FAIL);
			response.setStatus(Constants.STATUS_FAIL);
			throw new BusinessException(ErrorEnumUtil.ERROR_REGISTRAR_TASK);

		}

		response.setMessage(Constants.SUCCESS);
		response.setStatus(Constants.STATUS_OK);
		response.setData(data);

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_LISTA_TASK_SERVICE.getCode(), response);

		return response;
	}

	/**
	 * delete tarea en modelo H2
	 * 
	 * @param <T>
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public <T> RegistrationTaskResponse delTask(Long id) throws ParseException {

		RegistrationTaskResponse response = new RegistrationTaskResponse();

		try {

			boolean existe = registrationUserRepository.existsById(id);
			if (existe) {
				registrationUserRepository.deleteById(id);

				response.setMessage(Constants.SUCCESS_DEL);
				response.setStatus(Constants.STATUS_OK);
			} else {

				response.setMessage(Constants.REGISTER_NOT_FOUND);
				response.setStatus(Constants.STATUS_NOT_FOUND);
			}
			logger.info(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_DEL_TASK_SERVICE.getCode());

		} catch (DataAccessException ex) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_DEL_TASK_SERVICE.getCode(),
					ex.getLocalizedMessage());

			response.setMessage(Constants.FAIL);
			response.setStatus(Constants.STATUS_FAIL);
			throw new BusinessException(ErrorEnumUtil.ERROR_REGISTRAR_TASK);

		}

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_REGISTRA_TASK_SERVICE.getCode(), response);

		return response;
	}

	/**
	 * actualiza tarea en modelo H2
	 * 
	 * @param <T>
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public <T> RegistrationTaskResponse updateTask(Long id, TaskEntity task) throws ParseException {

		RegistrationTaskResponse response = new RegistrationTaskResponse();

		try {

			boolean existe = registrationUserRepository.existsById(id);
			if (existe) {
				TaskEntity taskEntity = new TaskEntity();
				taskEntity.setId(id);
				taskEntity.setDescription(task.getDescription());
				taskEntity.setCreationDate(task.getCreationDate());
				taskEntity.setActive(task.isActive());
				registrationUserRepository.save(taskEntity);

				response.setMessage(Constants.SUCCESS_UPDATE);
				response.setStatus(Constants.STATUS_OK);

			} else {

				response.setMessage(Constants.REGISTER_NOT_FOUND);
				response.setStatus(Constants.STATUS_NOT_FOUND);
			}
			logger.info(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_UPDATE_TASK_SERVICE.getCode());

		} catch (DataAccessException ex) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_UPDATE_TASK_SERVICE.getCode(),
					ex.getLocalizedMessage());

			response.setMessage(Constants.FAIL);
			response.setStatus(Constants.STATUS_FAIL);
			throw new BusinessException(ErrorEnumUtil.ERROR_REGISTRAR_TASK);

		}

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_UPDATE_TASK_SERVICE.getCode(), response);

		return response;
	}

}
