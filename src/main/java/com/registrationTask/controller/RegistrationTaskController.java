package com.registrationTask.controller;

import java.text.ParseException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.registrationTask.dto.GenericDtoResponse;
import com.registrationTask.dto.RegistrationTaskResponse;
import com.registrationTask.entity.TaskEntity;
import com.registrationTask.service.RegistrationTaskService;
import com.registrationTask.utils.InfoEnumUtil;
import com.registrationTask.utils.LogMessageUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase para registrar Tareas
 * 
 * @author fpainenao
 * 
 */
@RestController
@Api(value = "RegistrationTaskController")
public class RegistrationTaskController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationTaskController.class);

	@Autowired
	private RegistrationTaskService registrationTaskService;

	/**
	 * Metodo encargado de registrar Tarea
	 * 
	 * @param TaskRequest
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Registra Tareas")
	@PostMapping("/registrar/tarea")
	@ApiResponses(value = { @ApiResponse(code = 201, message = " Tarea Registrada con éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> addTask(@Valid @RequestBody(required = true) TaskEntity request)
			throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_REGISTRA_TASK.getCode(),
				"/registra/tarea", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		RegistrationTaskResponse lDtor = registrationTaskService.addTask(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_REGISTRA_TASK.getCode(),
				"/registrar/tarea", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

	/**
	 * Metodo encargado de listar Tareas
	 * 
	 * @param
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Listar Tareas")
	@GetMapping("/listar/tarea")
	@ApiResponses(value = { @ApiResponse(code = 201, message = " Tarea Registrada con éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> listarAllTask() throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_LISTA_TASK.getCode(), "/listar/tarea",
				LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		RegistrationTaskResponse lDtor = registrationTaskService.listAllTask();

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_LISTA_TASK.getCode(), "/listar/tarea",
				LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

	/**
	 * Metodo encargado de eliminar Tareas
	 * 
	 * @param TaskRequest
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Eliminar Tareas")
	@DeleteMapping("/eliminar/tarea/{id:[\\d]+}")
	@ApiResponses(value = { @ApiResponse(code = 201, message = " eliminar tareas con éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> eliminarTask(@PathVariable(value = "id") Long id) throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_DEL_TASK.getCode(), "/eliminar/tarea",
				LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		RegistrationTaskResponse lDtor = registrationTaskService.delTask(id);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_DEL_TASK.getCode(), "/eliminar/tarea",
				LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

	/**
	 * Metodo encargado de Actualizar Tarea
	 * 
	 * @param TaskRequest
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Actualiza Tarea")
	@PatchMapping("/actualiza/tarea/{id:[\\d]+}")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Actualiza tareas con éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> actualizaTask(@PathVariable(value = "id") Long id,
			@Valid @RequestBody TaskEntity task) throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_UPDATE_TASK.getCode(), "/actualiza/tarea",
				LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		RegistrationTaskResponse lDtor = registrationTaskService.updateTask(id, task);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_UPDATE_TASK.getCode(),
				"/actualiza/tarea", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

}
