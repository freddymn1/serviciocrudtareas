package com.registrationTask.entity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.registrationTask.controller.RegistrationTaskController;
import com.registrationTask.exception.BusinessException;
import com.registrationTask.repository.RegistrationTaskRepository;
import com.registrationTask.service.RegistrationTaskService;
import com.registrationTask.utils.Constants;
import com.registrationTask.utils.Utils;


@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(value = RegistrationTaskController.class)
@AutoConfigureMockMvc
public class TaskEntityTest {

	@Mock
	RegistrationTaskRepository registrationTaskRepository;

	@InjectMocks
	@Spy
	RegistrationTaskService registrationTaskService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private RegistrationTaskController registrationTaskController;
	
	 @Autowired
	    public ObjectMapper objectMapper;


	@Test
	public <T> void insertaTareas() throws ParseException {

		SimpleDateFormat dt1 = new SimpleDateFormat(Constants.FORMATO_FECHA);
		Long id = (long) 1000;
		TaskEntity taskEntity = new TaskEntity();
		taskEntity.setId(id);
		taskEntity.setActive(true);
		taskEntity.setDescription("Prueba1");
		taskEntity.setCreationDate(
				dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(Utils.fechaRegistro())));
		try {
			registrationTaskRepository.save(taskEntity);
		} catch (BusinessException e) {
			assert (Boolean.TRUE);
		}
		taskEntity.setId(taskEntity.getId());
		assertThat((T) registrationTaskRepository.findById(id)).isNotNull();

	}

	@Test
	public <T> void listarTareas() throws ParseException {

		assertThat((T) registrationTaskRepository.findTask()).isNotNull();

	}
	
	
	/**
     * Genera una prueba unitaria de login
     * 
     * @throws Exceptiont
     */
    @Test
    public void generateLoginIsValidShouldGenerateLogin() {
	try {
	    mockMvc.perform(post("/registrar/tarea").contentType(MediaType.APPLICATION_JSON)
		    .content(objectMapper.writeValueAsString(this.generateDtoRequest())));
	} catch (Exception e) {
	    fail("No deberia generar una excepcion");
	}
    }
	
    
    /**
     * Genera una objeto con las credenciales de login
     * 
     * @return
     * @throws ParseException 
     */
    public TaskEntity generateDtoRequest() throws ParseException {

    	TaskEntity taskEntity = new TaskEntity();
    	SimpleDateFormat dt1 = new SimpleDateFormat(Constants.FORMATO_FECHA);
		Long id = (long) 1000;
		taskEntity.setId(id);
		taskEntity.setActive(true);
		taskEntity.setDescription("Prueba1");
		taskEntity.setCreationDate(
				dt1.parse(new SimpleDateFormat(Constants.FORMATO_FECHA).format(Utils.fechaRegistro())));

	return taskEntity;
    }

}
